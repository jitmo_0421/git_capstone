// Dependencies
const mongoose = require('mongoose');

// User Schema
const wishSchema = new mongoose.Schema(
  {
    listing_id: {
      type: String,
      required: true,
    },
    user_id: {
      type: String,
      required: true,
    },
    ref_id: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model('Wishes', wishSchema);

// Dependencies
const express = require('express');

// Routing Components
const router = express.Router();

// Controllers
const orderController = require('../controllers/orderController.js');
const auth = require('../auth.js');

// Destructure Auth
const { verify, verifyAdmin } = auth;

// Checkout Many
router.post('/checkout', verify, orderController.checkOutMany);

// Checkout Single
router.post('/:id/checkout', verify, orderController.specificCheckOut);

// View All Orders
router.post('/view', verify, orderController.viewAllOrders);

// View Single Order
router.get('/view/:id', verify, orderController.viewOneOrder);

// View All Order (Admin)
router.get('/all', verify, verifyAdmin, orderController.allOrder);

// View Specific Order (Admin)
router.get('/view/:id/details', verify, verifyAdmin, orderController.specificOrder);

// View User Orders (Admin)
router.get('/view/:id/user', verify, verifyAdmin, orderController.userOrder);

// Update Order Status
router.put('/:id/status', verify, verifyAdmin, orderController.updateStatus);

// Cancel Pending Order
router.post('/:id/cancel', verify, orderController.cancelOrder);

// Refund/Return Order
router.post('/:id/return', verify, orderController.returnOrder);

// View Return or Refund Request (Admin)
router.get('/returns', verify, verifyAdmin, orderController.returnRequests);

// View Cancellation Request (Admin)
router.get('/cancel', verify, verifyAdmin, orderController.cancelRequests);

// View Pending Orders (Admin)
router.get('/pending', verify, verifyAdmin, orderController.pendingOrders);

// View Returned/Refunded Orders (Admin)
router.get('/refunded', verify, verifyAdmin, orderController.refundedOrders);

// View Completed Orders (Admin)
router.get('/completed', verify, verifyAdmin, orderController.completedOrders);

// View Cancelled Orders (Admin)
router.get('/cancelled', verify, verifyAdmin, orderController.cancelledOrders);

// View Processed Orders (Admin)
router.get('/processed', verify, verifyAdmin, orderController.processedOrders);

// Summary Report
router.get('/summary', verify, verifyAdmin, orderController.summaryReport);

module.exports = router;

const express = require('express');
const listingController = require('../controllers/listingController');
const { verifyToken, verifyAdmin } = require('../utils/token');

const router = express.Router();

// Create Listing
router.post('/create', verifyToken, listingController.createList);

// Deactivate Listing
router.put('/archive/:id', verifyToken, listingController.deactivateListing);

// Update Listing
router.post('/update/:id', verifyToken, listingController.updateListing);

// Get User's Listing
router.get('/get/:id', listingController.getListing);

// Get All Active Search Listings
router.get('/get', listingController.getActiveListings);

// Get All Listings
router.get('/all', verifyToken, verifyAdmin, listingController.getAllListings);

// Change Listing State
router.put('/change/:action/:id', verifyToken, verifyAdmin, listingController.changeState);

module.exports = router;

// Model Imports
const User = require('../models/User');
const Listing = require('../models/Listing');

// Utils Imports
const bcryptjs = require('bcryptjs');
const { errorHandler } = require('../utils/error');

// Update User
const updateUser = async (req, res, next) => {
  const displayName =
    req.body.firstName && req.body.lastName ? `${req.body.firstName} ${req.body.lastName}` : '';
  if (req.user.id !== req.params.id) {
    return next(errorHandler(401, 'You are not authorized to do this action'));
  }
  try {
    if (req.body.password) {
      req.body.password = bcryptjs.hashSync(req.body.password, 10);
    }

    const updateUser = await User.findByIdAndUpdate(
      req.params.id,
      {
        $set: {
          firstName: req.body.firstName,
          lastName: req.body.lastName,
          displayName: displayName,
          password: req.body.password,
          contactNumber: req.body.contactNumber,
          avatar: req.body.avatar,
        },
      },
      { new: true }
    );
    const { password, ...rest } = updateUser._doc;
    res.status(200).json(rest);
  } catch (error) {
    next(error);
  }
};

// Deactivate User
const deleteUser = async (req, res, next) => {
  if (req.user.id !== req.params.id) return next(errorHandler(401, 'Action is Forbidden'));

  try {
    await User.findByIdAndUpdate(req.params.id, { isActive: false });
    res.clearCookie('access_token');
    res.status(200).json('User has been deactivated');
  } catch (error) {
    next(error);
  }
};

const getUserListings = async (req, res, next) => {
  if (req.user.id === req.params.id) {
    try {
      const listings = await Listing.find({ userRef: req.params.id, isActive: true });
      res.status(200).json(listings);
    } catch (error) {
      next(error);
    }
  } else {
    return next(errorHandler(401, 'You can only view your own listings!'));
  }
};

// Get all users
const getAllUsers = async (req, res, next) => {
  try {
    const users = await User.find({});

    return res.status(200).json(users);
  } catch (error) {
    next(error);
  }
};

// Change State
const changeState = async (req, res, next) => {
  const id = req.params.id;
  const action = req.params.action;
  try {
    const users = await User.findByIdAndUpdate({ _id: id }, { isActive: action });
    console.log(users);
    res.status(200).json(users);
  } catch (error) {
    next(error);
  }
};

// View User
const viewUser = async (req, res, next) => {
  try {
    console.log(req.params.id);
    const data = await User.findOne(
      { _id: req.params.id, isActive: true },
      { password: 0, isAdmin: 0, __v: 0, createdAt: 0, updatedAt: 0 }
    );
    res.status(200).json(data);
  } catch (error) {
    next(error);
  }
};

module.exports = {
  updateUser: updateUser !== undefined ? updateUser : null,
  deleteUser: deleteUser !== undefined ? deleteUser : null,
  getUserListings: getUserListings !== undefined ? getUserListings : null,
  getAllUsers: getAllUsers !== undefined ? getAllUsers : null,
  changeState: changeState !== undefined ? changeState : null,
  viewUser: viewUser !== undefined ? viewUser : null,
};

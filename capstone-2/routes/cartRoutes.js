// Dependencies
const express = require('express');

// Routing Components
const router = express.Router();

// Controllers
const cartController = require('../controllers/cartController.js');
const auth = require('../auth.js');

// Destructure Auth
const { verify, verifyAdmin } = auth;

// Add to Cart
router.post('/addToCart', verify, cartController.addToCart);

// View Cart
router.post('/viewCart', verify, cartController.viewCart);

// Deduct Quantity from Cart
router.put('/viewCart/:id', verify, cartController.deductQuantity);

// Remove all Carted
router.get('/viewCart/remove-all', verify, cartController.deleteAll);

// Remove Single Item
router.get('/view/:id/delete', verify, cartController.deleteOne);

// Exports
module.exports = router;

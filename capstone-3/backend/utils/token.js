// Dependencies
const jwt = require('jsonwebtoken');
require('dotenv').config();
const { errorHandler } = require('./error');

// SECRET key from .env
const SECRET_KEY = process.env.KEY;

// Create Token
const createAccessToken = (user) => {
  const userData = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };
  return jwt.sign(userData, SECRET_KEY);
};

// Verify User via Cookies
const verifyToken = (req, res, next) => {
  const token = req.cookies.access_token;

  if (!token) return next(errorHandler(401, 'Unauthorized Action'));
  jwt.verify(token, process.env.KEY, (err, user) => {
    if (err) return next(errorHandler(403, 'Forbidden'));
    req.user = user;
    next();
  });
};

// Retreive token from the request header
const verify = (req, res, next) => {
  // Auth Type is Bearer Token

  let token = req.headers.authorization;

  if (typeof token === 'undefined') {
    return res.send({ auth: 'Failed, No Token!' });
  } else {
    token = token.slice(7, token.length);
    jwt.verify(token, SECRET_KEY, function (error, decodedToken) {
      if (error) {
        return res.send({
          auth: 'Failed',
          message: error.message,
        });
      } else {
        req.user = decodedToken;

        // Will let the controller to proceed next
        next();
      }
    });
  }
};

// Verify User Level
const verifyAdmin = (req, res, next) => {
  if (req.user.isAdmin) {
    next();
  } else {
    return res.send({
      auth: 'Failed!',
      message: 'Access Forbidden',
    });
  }
};

module.exports = {
  createAccessToken: createAccessToken !== undefined ? createAccessToken : null,
  verifyToken: verifyToken !== undefined ? verifyToken : null,
  verify: verify !== undefined ? verify : null,
  verifyAdmin: verifyAdmin !== undefined ? verifyAdmin : null,
};

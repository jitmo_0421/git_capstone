// Dependencies
const Products = require('../models/Products.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

// Add Products
const addProduct = async (req, res) => {
  let marginPrice;
  if (req.body.supplierPrice) {
    marginPrice = await (req.body.supplierPrice + req.body.supplierPrice * req.body.marginPrice);
  } else {
    marginPrice = await req.body.price;
  }
  const newProduct = await new Products({
    sku: req.body.sku,
    name: req.body.name,
    description: req.body.description,
    supplier: {
      supplierId: req.body.supplierId,
      supplierName: req.body.supplierName,
      supplierPrice: req.body.supplierPrice,
    },
    category: req.body.category,
    subCategory: req.body.subCategory,
    marginPrice: req.body.marginPrice,
    price: marginPrice,
    quantity: req.body.quantity,
  });

  await Products.find({ sku: req.body.sku }).then((product) => {
    if (product.length > 0) {
      return res.status(400).json({ error: 'SKU already exists' });
    } else {
      return newProduct
        .save()
        .then((newProduct, err) => {
          if (err) {
            return res.status(400).json({ error: 'Internal Server Error' });
          } else {
            return res.status(201).json({
              message: 'Product added successfully',
              product: {
                sku: newProduct.sku,
                name: newProduct.name,
                price: newProduct.price,
                quantity: newProduct.quantity,
              },
            });
          }
        })
        .catch((err) => {
          return res.status(400).json({ error: err.message });
        });
    }
  });
};

// Get All Products
const getAllProducts = (req, res) => {
  Products.find({}).then((products) => {
    if (products.length > 0) {
      return res.status(200).json({ products: products });
    } else {
      return res.status(400).json({ error: 'No products found.' });
    }
  });
};

// Get Active Products
const getActiveProducts = (req, res) => {
  Products.find({ isActive: true }).then((products) => {
    if (products.length > 0) {
      return res.status(200).json({ products: products });
    } else {
      return res.status(400).json({ error: 'No active products found.' });
    }
  });
};

// Get Archived Products
const getArchivedProducts = (req, res) => {
  Products.find({ isActive: false }).then((products) => {
    if (products.length > 0) {
      return res.status(200).json({ products: products });
    } else {
      return res.status(400).json({ error: 'No archived products found.' });
    }
  });
};

// Get Specific Product
const searchProduct = (req, res) => {
  Products.find({ _id: req.body.id, isActive: true }).then((products) => {
    if (products.length > 0) {
      return res.status(200).json({ products: products });
    } else {
      return res.status(400).json({ error: 'No products found.' });
    }
  });
};

// Update Product
const updateProduct = (req, res) => {
  const updateProduct = {
    sku: req.body.sku,
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
    quantity: req.body.quantity,
  };
  Products.findByIdAndUpdate(req.params.id, updateProduct).then((products, err) => {
    if (err) {
      return res.status(400).json({ error: 'Internal Server Error' });
    } else {
      return res.status(200).json({
        message: 'Product updated successfully',
        product: {
          sku: products.sku,
          name: products.name,
          description: products.description,
          price: products.price,
          quantity: products.quantity,
        },
      });
    }
  });
};

// Archive Product
const archiveProduct = (req, res) => {
  Products.findById(req.params.id).then((product) => {
    if (product.isActive === true) {
      Products.findByIdAndUpdate(req.params.id, { isActive: false }).then((product, err) => {
        if (err) {
          return res.status(400).json({ error: 'Internal Server Error' });
        } else {
          return res.status(200).json({
            message: 'Product archived successfully',
            product: {
              sku: product.sku,
              name: product.name,
              description: product.description,
              price: product.price,
              quantity: product.quantity,
            },
          });
        }
      });
    } else {
      return res.status(400).json({ error: 'Product is already in the archives.' });
    }
  });
};

// Activate Product
const activateProduct = (req, res) => {
  Products.findById(req.params.id).then((product) => {
    if (product.isActive === false) {
      Products.findByIdAndUpdate(req.params.id, { isActive: true }).then((product, err) => {
        if (err) {
          return res.status(400).json({ error: 'Internal Server Error' });
        } else {
          return res.status(200).json({
            message: 'Product successfully activated.',
            product: {
              sku: product.sku,
              name: product.name,
              description: product.description,
              price: product.price,
              quantity: product.quantity,
            },
          });
        }
      });
    } else {
      return res.status(400).json({ error: 'This product is already active.' });
    }
  });
};

// View Products by Category
const viewProductsByCategory = (req, res) => {
  Products.find({ category: new RegExp(`^${req.params.category}$`, 'i'), isActive: true }).then(
    (products) => {
      if (products.length > 0) {
        return res.status(200).json({ products: products });
      } else {
        return res.status(400).json({ error: 'No products found.' });
      }
    }
  );
};

// View Products by Subcategory
const viewProductsBySub = (req, res) => {
  Products.find({
    category: new RegExp(`^${req.params.category}$`, 'i'),
    subCategory: new RegExp(`^${req.params.subcategory}$`, 'i'),
    isActive: true,
  }).then((products) => {
    if (products.length > 0) {
      return res.status(200).json({ products: products });
    } else {
      return res.status(400).json({ error: 'No products found.' });
    }
  });
};

module.exports = {
  addProduct: addProduct !== undefined ? addProduct : null,
  getAllProducts: getAllProducts !== undefined ? getAllProducts : null,
  getActiveProducts: getActiveProducts !== undefined ? getActiveProducts : null,
  getArchivedProducts: getArchivedProducts !== undefined ? getArchivedProducts : null,
  searchProduct: searchProduct !== undefined ? searchProduct : null,
  updateProduct: updateProduct !== undefined ? updateProduct : null,
  archiveProduct: archiveProduct !== undefined ? archiveProduct : null,
  activateProduct: activateProduct !== undefined ? activateProduct : null,
  viewProductsByCategory: viewProductsByCategory !== undefined ? viewProductsByCategory : null,
  viewProductsBySub: viewProductsBySub !== undefined ? viewProductsBySub : null,
};

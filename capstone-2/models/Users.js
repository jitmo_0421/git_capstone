// Dependencies
const mongoose = require('mongoose');

// User Schema
const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: [true, 'e-mail is required'],
  },
  firstName: {
    type: String,
    required: [true, 'first name is required'],
  },
  lastName: {
    type: String,
    required: [true, 'last name is required'],
  },
  password: {
    type: String,
    required: [true, 'password is required'],
  },
  address: [
    {
      brgy: {
        type: String,
        required: [true, 'barangay is required'],
      },
      city: {
        type: String,
        required: [true, 'city is required'],
      },
      province: {
        type: String,
        required: [true, 'province is required'],
      },
    },
  ],
  contactNumber: {
    type: String,
    required: [true, 'contact number is required'],
  },
  avatar: {
    type: String,
    default: 'https://res.cloudinary.com/da3lvrezp/image/upload/v1696844937/uploads/avatar_dqm8ay.jpg',
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
});

module.exports = mongoose.model('Users', userSchema);

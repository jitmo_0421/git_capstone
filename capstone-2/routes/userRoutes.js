// Dependencies
const express = require('express');

// Routing Components
const router = express.Router();

// Controllers
const userController = require('../controllers/userController.js');
const auth = require('../auth.js');

// Destructure Auth
const { verify, verifyAdmin } = auth;

// User Registration Route
router.post('/register', userController.userRegister);

// User Login Route
router.post('/login', userController.userLogin);

// Retrieve User Details
router.get('/:id/user', verify, userController.viewProfile);

// Retrieve Own Profile
router.get('/profile', verify, userController.myProfile);

// Reset Password
router.put('/reset-password', verify, userController.resetPassword);

// Add Admin User
router.post('/admin/add', verify, verifyAdmin, userController.adminRegister);

// View All Admin Users
router.get('/admin/all', verify, verifyAdmin, userController.viewAdmin);

// Change User Role
router.put('/admin/role', verify, verifyAdmin, userController.changeRole);

// Retrieve All Users
router.get('/all', verify, verifyAdmin, userController.viewAllUsers);

// Exports
module.exports = router;

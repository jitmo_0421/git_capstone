const express = require('express');

// Controller
const authController = require('../controllers/authController');

const router = express.Router();

// User Signup
router.post('/signup', authController.signup);

// User Signin
router.post('/signin', authController.signin);

// Google Signin
router.post('/google', authController.google);

// Signout
router.get('/signout', authController.signOut);

module.exports = router;

// Dependencies
const Users = require('../models/Users.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

// Register User
const userRegister = async (req, res) => {
  const newUser = new Users({
    email: req.body.email,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    password: await bcrypt.hash(req.body.password, 10),
    address: {
      brgy: req.body.brgy,
      city: req.body.city,
      province: req.body.province,
    },
    contactNumber: req.body.contactNumber,
  });

  Users.find({ email: req.body.email }).then((user) => {
    if (user.length > 0) {
      return res.status(400).json({ error: 'E-mail already exists' });
    } else {
      return newUser
        .save()
        .then((newUser, err) => {
          if (err) {
            return res.status(400).json({ error: 'Internal Server Error' });
          } else {
            return res.status(201).json({ message: 'User registered successfully' });
          }
        })
        .catch((err) => {
          return res.status(400).json({ error: err.message });
        });
    }
  });
};

// User Login
const userLogin = async (req, res) => {
  return Users.findOne({ email: req.body.email })
    .then((user) => {
      if (user === null) {
        return res.status(401).send('Invalid Credentials! Please Try Again!');
      } else {
        const isPasswordCorrect = bcrypt.compareSync(req.body.password, user.password);
        if (isPasswordCorrect) {
          return res.send({ access: auth.createAccessToken(user) });
        } else {
          return res.send(false);
        }
      }
    })
    .catch((err) => err);
};

// Retrieve User Details
const viewProfile = async (req, res) => {
  return Users.findById(req.params.id, { __v: 0 })
    .then((result) => {
      result.password = 'hidden';
      return res.send(result);
    })
    .catch((error) => {
      if (error.kind) {
        return res.status(400).send({ error: 'User Not Found!' });
      } else {
        return res.status(500).send({ error: 'Internal Server Error!' });
      }
    });
};

// Retrieve Own Profile
const myProfile = async (req, res) => {
  return Users.findById(req.user.id, { __v: 0 })
    .then((result) => {
      result.password = 'hidden';
      return res.send(result);
    })
    .catch((error) => {
      if (error.kind) {
        return res.status(400).send({ error: 'User Not Found!' });
      } else {
        return res.status(500).send({ error: 'Internal Server Error!' });
      }
    });
};

const resetPassword = async (req, res) => {
  try {
    const newPassword = req.body.newPassword;
    const userId = req.user.id;

    // Hashing Password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    await Users.findByIdAndUpdate(userId, { password: hashedPassword });

    res.status(200).json({ message: 'Password has been reset successfully' });
  } catch (err) {
    return res.status(500).json({ message: err.message });
  }
};

// Register Admin User
const adminRegister = async (req, res) => {
  const newUser = new Users({
    email: req.body.email,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    password: await bcrypt.hash(req.body.password, 10),
    address: {
      brgy: req.body.brgy,
      municipality: req.body.municipality,
      city: req.body.city,
      province: req.body.province,
    },
    contactNumber: req.body.contactNumber,
    isAdmin: true,
  });

  Users.find({ email: req.body.email }).then((user) => {
    if (user.length > 0) {
      return res.status(400).json({ error: 'E-mail already exists' });
    } else {
      return newUser
        .save()
        .then((newUser, err) => {
          if (err) {
            return res.status(400).json({ error: 'Internal Server Error' });
          } else {
            return res.status(201).json({ message: 'User Registered Successfully' });
          }
        })
        .catch((err) => {
          return res.status(400).json({ error: err.message });
        });
    }
  });
};

// View Admin Users
const viewAdmin = async (req, res) => {
  return Users.find({ isAdmin: true })
    .then((result) => {
      return res.send(result);
    })
    .catch((error) => {
      if (error.kind) {
        return res.status(400).send({ error: 'User Not Found!' });
      } else {
        return res.status(500).send({ error: 'Internal Server Error!' });
      }
    });
};

// Change User Role
const changeRole = async (req, res) => {
  try {
    const userId = req.body.userId;
    const role = req.body.isAdmin;
    return await Users.findByIdAndUpdate(userId, { isAdmin: role }).then((result, err) => {
      if (err) {
        return res.status(400).json({ error: 'Internal Server Error' });
      } else {
        return res.status(200).json({ message: 'Role Changed Successfully' });
      }
    });
  } catch (err) {
    return res.status(500).json({ message: err.message });
  }
};

// View All Users
const viewAllUsers = async (req, res) => {
  return Users.find()
    .then((result) => {
      return res.send(result);
    })
    .catch((error) => {
      if (error.kind) {
        return res.status(400).send({ error: 'User Not Found!' });
      } else {
        return res.status(500).send({ error: 'Internal Server Error!' });
      }
    });
};

module.exports = {
  userRegister: userRegister !== undefined ? userRegister : null,
  userLogin: userLogin !== undefined ? userLogin : null,
  viewProfile: viewProfile !== undefined ? viewProfile : null,
  myProfile: myProfile !== undefined ? myProfile : null,
  resetPassword: resetPassword !== undefined ? resetPassword : null,
  adminRegister: adminRegister !== undefined ? adminRegister : null,
  viewAdmin: viewAdmin !== undefined ? viewAdmin : null,
  changeRole: changeRole !== undefined ? changeRole : null,
  viewAllUsers: viewAllUsers !== undefined ? viewAllUsers : null,
};

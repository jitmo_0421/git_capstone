// Dependencies
const Cart = require('../models/Cart.js');
const Orders = require('../models/Orders.js');
const Products = require('../models/Products.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

// Check out Many
const checkOutMany = async (req, res) => {
  try {
    if (req.user.isAdmin) {
      return res.send('Hey Admin! You cannot do this action.');
    }
    const cart = await Cart.find(
      { userId: req.user.id },
      { _id: 0, userId: 0, description: 0, cartedOn: 0, __v: 0 }
    ).then(async (cart) => {
      const totalAmount = await cart.reduce((acc, product) => acc + product.subTotal, 0);
      const newOrder = new Orders({
        userId: req.user.id,
        products: cart,
        totalAmount: totalAmount,
      });
      for (let i = 0; i < cart.length; i++) {
        const productId = cart[i].productId;
        const updatedQuantity = cart[i].quantity;
        await Products.findById(productId).then(async (product, err) => {
          const newQuantity = product.quantity - updatedQuantity;
          await Products.findByIdAndUpdate(productId, { quantity: newQuantity });
        });
      }
      await newOrder.save().then(async (order, err) => {
        if (err) {
          res.status(500).json({ message: 'Internal Server Error' });
        } else {
          await Cart.deleteMany({ userId: req.user.id });
          res.status(201).send('Order successfully placed');
        }
      });
    });
  } catch (err) {
    console.log(err);
  }
};

//Check Out Specific Product
const specificCheckOut = async (req, res) => {
  try {
    if (req.user.isAdmin) {
      return res.send('Hey Admin! You cannot do this action.');
    }
    await Cart.findOne(
      { userId: req.user.id, productId: req.params.id },
      { _id: 0, userId: 0, description: 0, cartedOn: 0, __v: 0 }
    ).then(async (cart, err) => {
      const newOrder = new Orders({
        userId: req.user.id,
        products: cart,
        totalAmount: cart.subTotal,
      });
      await Products.findByIdAndUpdate(req.params.id).then(async (product, err) => {
        const newQuantity = product.quantity - cart.quantity;
        await Products.findByIdAndUpdate(req.params.id, { quantity: newQuantity });
      });
      await newOrder.save().then(async (order, err) => {
        if (err) {
          res.status(500).json({ message: 'Internal Server Error' });
        } else {
          await Cart.deleteOne({ userId: req.user.id, productId: req.params.id });
          res.status(201).send('Order successfully placed');
        }
      });
    });
  } catch (err) {
    console.log(err);
  }
};

// View All Orders
const viewAllOrders = (req, res) => {
  try {
    if (req.user.isAdmin) {
      return res.send('Hey Admin! You cannot do this action.');
    }
    Orders.find({ userId: req.user.id, status: { $nin: ['Completed', 'Cancelled', 'Refunded'] } }, { __v: 0 })
      .then((orders) => {
        if (orders.length > 0) {
          res.status(200).json({ orders: orders });
        } else {
          res.status(404).json({ message: 'No Orders Found!' });
        }
      })
      .catch((err) => {
        res.status(500).json({ message: 'Internal Server Error' });
      });
  } catch (err) {
    console.log(err);
  }
};

// View Single Order
const viewOneOrder = async (req, res) => {
  try {
    if (req.user.isAdmin) {
      return res.send('Hey Admin! You cannot do this action.');
    }
    Orders.find({ _id: req.params.id, userId: req.user.id }, { __v: 0 })
      .then((order) => {
        if (order) {
          res.status(200).json({ order: order });
        } else {
          res.status(404).json({ message: 'Order Not Found!' });
        }
      })
      .catch((err) => {
        res.status(500).json({ message: 'Internal Server Error' });
      });
  } catch (err) {
    console.log(err);
  }
};

// Update Order Status
const updateStatus = async (req, res) => {
  try {
    const status = {
      status: req.body.status,
    };
    Orders.findByIdAndUpdate(req.params.id, status).then((order, err) => {
      if (err) {
        res.status(500).json({ message: 'Internal Server Error' });
      } else {
        res.status(200).json({ message: 'Order Status Updated!' });
      }
    });
  } catch (err) {
    if (err.kind) {
      res.status(404).send({ message: 'Order Not Found!' });
    } else {
      res.status(500).send({ message: 'Internal Server Error!' });
    }
  }
};

// Cancel Pending Order
const cancelOrder = async (req, res) => {
  try {
    if (req.user.isAdmin) {
      return res.send('Hey Admin! You cannot do this action.');
    }
    await Orders.find({ _id: req.params.id, userId: req.user.id }).then(async (orders) => {
      if (orders[orders.length - 1].status === 'Pending') {
        if (orders.length > 0) {
          Orders.findOneAndUpdate(
            { _id: req.params.id, userId: req.user.id },
            { status: 'Request Cancellation' }
          ).then((cancel, err) => {
            if (err) {
              res.status(500).json({ message: 'Internal Server Error' });
            } else {
              res.status(200).json({ message: 'Order Cancellation Request! Please wait for approval' });
            }
          });
        } else {
          res.status(404).json({ message: 'Order Not Found!' });
        }
      } else if (orders[orders.length - 1].status === 'Request Cancellation') {
        res.status(200).send({ message: 'On-going Request Cancellation! Please wait for approval!' });
      } else if (orders[orders.length - 1].status !== 'Pending') {
        res.status(404).json({ message: 'Unable to proceed with this request!' });
      } else {
        res.status(404).json({ message: 'Order cannot be cancelled because it is already processed!' });
      }
    });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

// Return/Refund Order
const returnOrder = async (req, res) => {
  try {
    if (req.user.isAdmin) {
      return res.send('Hey Admin! You cannot do this action.');
    }
    await Orders.find({ _id: req.params.id, userId: req.user.id }).then(async (orders) => {
      if (orders[orders.length - 1].status === 'Completed') {
        if (orders.length > 0) {
          Orders.findOneAndUpdate(
            { _id: req.params.id, userId: req.user.id },
            { status: 'Return/Refund Request' }
          ).then((cancel, err) => {
            if (err) {
              res.status(500).json({ message: 'Internal Server Error' });
            } else {
              res.status(200).json({ message: 'Return/Refund Request! Please wait for approval' });
            }
          });
        } else {
          res.status(404).json({ message: 'Order Not Found!' });
        }
      } else if (orders[orders.length - 1].status === 'Return or Refund Request') {
        res.status(200).send({ message: 'On-going Return and Refund Request! Please wait for approval!' });
      } else if (orders[orders.length - 1].status !== 'Completed') {
        res.status(404).json({ message: 'Unable to proceed with this request!' });
      } else {
        res.status(404).json({ message: 'Invalid Request' });
      }
    });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

// View All Order (Admin)
const allOrder = async (req, res) => {
  try {
    if (!req.user.isAdmin) {
      return res.status(401).json({ message: 'Unauthorized' });
    }
    Orders.find({}).then((orders) => {
      if (orders.length > 0) {
        res.status(200).json({ orders: orders });
      } else {
        res.status(404).json({ message: 'No Orders Found!' });
      }
    });
  } catch (err) {
    res.status(500).json({ message: 'Internal Server Error' });
  }
};

// View Specific Order (Admin)
const specificOrder = async (req, res) => {
  try {
    if (!req.user.isAdmin) {
      return res.status(401).json({ message: 'Unauthorized' });
    }
    Orders.findById(req.params.id).then((order) => {
      if (order) {
        res.status(200).json({ order: order });
      } else {
        res.status(404).json({ message: 'Order Not Found!' });
      }
    });
  } catch (err) {
    res.status(500).json({ message: 'Internal Server Error' });
  }
};

// View All User Orders (Admin)
const userOrder = async (req, res) => {
  try {
    if (!req.user.isAdmin) {
      return res.status(401).json({ message: 'Unauthorized' });
    }
    Orders.find({ userId: req.params.id }).then((orders) => {
      if (orders.length > 0) {
        res.status(200).json({ orders: orders });
      } else {
        res.status(404).json({ message: 'No Orders Found!' });
      }
    });
  } catch (err) {
    res.status(500).json({ message: 'Internal Server Error' });
  }
};

// View Return Requests (Admin)
const returnRequests = async (req, res) => {
  try {
    if (!req.user.isAdmin) {
      return res.status(401).json({ message: 'Unauthorized' });
    }
    Orders.find({ status: 'Return or Refund Request' }).then((orders) => {
      if (orders.length > 0) {
        res.status(200).json({ orders: orders });
      } else {
        res.status(404).json({ message: 'No Orders Found!' });
      }
    });
  } catch (err) {
    res.status(500).json({ message: 'Internal Server Error' });
  }
};

// View Cancel Requests (Admin)
const cancelRequests = async (req, res) => {
  try {
    if (!req.user.isAdmin) {
      return res.status(401).json({ message: 'Unauthorized' });
    }
    Orders.find({ status: 'Request Cancellation' }).then((orders) => {
      if (orders.length > 0) {
        res.status(200).json({ orders: orders });
      } else {
        res.status(404).json({ message: 'No Orders Found!' });
      }
    });
  } catch (err) {
    res.status(500).json({ message: 'Internal Server Error' });
  }
};

// View Pending Orders (Admin)
const pendingOrders = async (req, res) => {
  try {
    if (!req.user.isAdmin) {
      return res.status(401).json({ message: 'Unauthorized' });
    }
    Orders.find({ status: 'Pending' }).then((orders) => {
      if (orders.length > 0) {
        res.status(200).json({ orders: orders });
      } else {
        res.status(404).json({ message: 'No Orders Found!' });
      }
    });
  } catch (err) {
    res.status(500).json({ message: 'Internal Server Error' });
  }
};

// View Completed Orders (Admin)
const completedOrders = async (req, res) => {
  try {
    if (!req.user.isAdmin) {
      return res.status(401).json({ message: 'Unauthorized' });
    }
    Orders.find({ status: 'Completed' }).then((orders) => {
      if (orders.length > 0) {
        res.status(200).json({ orders: orders });
      } else {
        res.status(404).json({ message: 'No Orders Found!' });
      }
    });
  } catch (err) {
    res.status(500).json({ message: 'Internal Server Error' });
  }
};

// View Refunded Orders (Admin)
const refundedOrders = async (req, res) => {
  try {
    if (!req.user.isAdmin) {
      return res.status(401).json({ message: 'Unauthorized' });
    }
    Orders.find({ status: { $in: ['Refunded', 'Returned'] } }).then((orders) => {
      if (orders.length > 0) {
        res.status(200).json({ orders: orders });
      } else {
        res.status(404).json({ message: 'No Orders Found!' });
      }
    });
  } catch (err) {
    res.status(500).json({ message: 'Internal Server Error' });
  }
};

// View Cancelled Orders (Admin)
const cancelledOrders = async (req, res) => {
  try {
    if (!req.user.isAdmin) {
      return res.status(401).json({ message: 'Unauthorized' });
    }
    Orders.find({ status: 'Cancelled' }).then((orders) => {
      if (orders.length > 0) {
        res.status(200).json({ orders: orders });
      } else {
        res.status(404).json({ message: 'No Orders Found!' });
      }
    });
  } catch (err) {
    res.status(500).json({ message: 'Internal Server Error' });
  }
};

// View Processed Orders (Admin)
const processedOrders = async (req, res) => {
  try {
    if (!req.user.isAdmin) {
      return res.status(401).json({ message: 'Unauthorized' });
    }
    Orders.find({
      status: {
        $nin: [
          'Pending',
          'Refunded',
          'Returned',
          'Cancelled',
          'Completed',
          'Request for Cancellation',
          'Return or Refund Request',
        ],
      },
    }).then((orders) => {
      if (orders.length > 0) {
        res.status(200).json({ orders: orders });
      } else {
        res.status(404).json({ message: 'No Orders Found!' });
      }
    });
  } catch (err) {
    res.status(500).json({ message: 'Internal Server Error' });
  }
};

// Summary Report
const summaryReport = async (req, res) => {
  try {
    const pending = await Orders.find({
      status: { $nin: ['Completed', 'Cancelled', 'Refunded', 'Request Cancellation'] },
    }).then(async (pending) => {
      const totalAmount = await pending.reduce((acc, product) => acc + product.totalAmount, 0);
      return await { pendingAmount: totalAmount, pendingCount: pending.length };
    });
    const completed = await Orders.find({
      status: { $eq: 'Completed' },
    }).then(async (completed) => {
      const totalAmount = await completed.reduce((acc, product) => acc + product.totalAmount, 0);
      return await { completedAmount: totalAmount, completedCount: completed.length };
    });
    const cancelled = await Orders.find({
      status: { $in: ['Cancelled'] },
    }).then(async (cancelled) => {
      const totalAmount = await cancelled.reduce((acc, product) => acc + product.totalAmount, 0);
      return { cancelledAmount: totalAmount, cancelledCount: cancelled.length };
    });
    const refund = await Orders.find({
      status: { $in: ['Refunded', 'Returned'] },
    }).then(async (refund) => {
      const totalAmount = await refund.reduce((acc, product) => acc + product.totalAmount, 0);
      return { cancelledAmount: totalAmount, cancelledCount: refund.length };
    });
    res.status(200).json({ pending: pending, completed: completed, cancelled: cancelled, refund: refund });
  } catch (err) {
    res.status(500).send({ message: err.message });
  }
};

// Exports
module.exports = {
  checkOutMany: checkOutMany !== undefined ? checkOutMany : null,
  specificCheckOut: specificCheckOut !== undefined ? specificCheckOut : null,
  viewAllOrders: viewAllOrders !== undefined ? viewAllOrders : null,
  updateStatus: updateStatus !== undefined ? updateStatus : null,
  cancelOrder: cancelOrder !== undefined ? cancelOrder : null,
  summaryReport: summaryReport !== undefined ? summaryReport : null,
  viewOneOrder: viewOneOrder !== undefined ? viewOneOrder : null,
  returnOrder: returnOrder !== undefined ? returnOrder : null,
  allOrder: allOrder !== undefined ? allOrder : null,
  userOrder: userOrder !== undefined ? userOrder : null,
  specificOrder: specificOrder !== undefined ? specificOrder : null,
  cancelOrder: cancelOrder !== undefined ? cancelOrder : null,
  returnOrder: returnOrder !== undefined ? returnOrder : null,
  pendingOrders: pendingOrders !== undefined ? pendingOrders : null,
  completedOrders: completedOrders !== undefined ? completedOrders : null,
  returnRequests: returnRequests !== undefined ? returnRequests : null,
  cancelRequests: cancelRequests !== undefined ? cancelRequests : null,
  refundedOrders: refundedOrders !== undefined ? refundedOrders : null,
  cancelledOrders: cancelledOrders !== undefined ? cancelledOrders : null,
  processedOrders: processedOrders !== undefined ? processedOrders : null,
};

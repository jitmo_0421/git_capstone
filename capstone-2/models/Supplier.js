// Dependencies
const mongoose = require('mongoose');

// Supplier Schema
const supplierSchema = new mongoose.Schema({
  supplierName: {
    type: String,
    required: [true, 'Please add a supplier name'],
  },
  supplierEmail: {
    type: String,
    required: [true, 'Please add a supplier email'],
  },
  supplierPhone: {
    type: String,
    required: [true, 'Please add a supplier phone'],
  },
  supplierAddress: [
    {
      street: {
        type: String,
        required: [true, 'Please add a supplier street'],
      },
      brgy: {
        type: String,
        required: [true, 'Please add a supplier brgy'],
      },
      municipality: {
        type: String,
        required: [true, 'Please add a supplier municipality'],
      },
      city: {
        type: String,
        required: [true, 'Please add a supplier city'],
      },
      province: {
        type: String,
        required: [true, 'Please add a supplier province'],
      },
    },
  ],
  supplierContactPerson: {
    type: String,
    required: [true, 'Please add a supplier contact person'],
  },
});

module.exports = mongoose.model('Suppliers', supplierSchema);

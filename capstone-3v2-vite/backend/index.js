// Server Dependencies
const express = require('express');
const mongoose = require('mongoose');
// const cors = require('cors');
const cookieParser = require('cookie-parser');

// Environment Variable
require('dotenv').config();

//Access Routes
const userRouter = require('./routes/userRoute');
const authRouter = require('./routes/authRoute');
const listingRouter = require('./routes/listingRoute');
const purchaseRouter = require('./routes/purchaseRoutes');
const wishRouter = require('./routes/wishRoute');

// Server Setup and Routes
const port = 4000; // Sub Port

const app = express();
app.use(express.urlencoded({ limit: '50mb', extended: true }));
app.use(express.json({ limit: '50mb' }));
// app.use(cors());
app.use(cookieParser());

// Database Connection
mongoose.connect(process.env.dbURI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

// Database Checkpoints
const dbConnection = mongoose.connection;
dbConnection.on('error', console.error.bind(console, 'connection error:'));
dbConnection.once('open', () => {
  console.log('Connected to Atlas Cloud Database');
});

// Back-end Routes
app.use('/api/user', userRouter); // User Router End-Point
app.use('/api/auth', authRouter); // Auth Router End-Point
app.use('/api/listing', listingRouter); // Listing Router End-Point
app.use('/api/purchase', purchaseRouter); // Purchase Router End-Point
app.use('/api/wishlist', wishRouter); // Purchase Router End-Point

// Error Handler
app.use((err, req, res, next) => {
  const statusCode = err.statusCode || 500;
  const message = err.message || 'Internal Server Error';
  return res.status(statusCode).json({
    success: false,
    statusCode,
    message,
  });
});

// Server Gateway Reponse
if (require.main === module) {
  app.listen(process.env.PORT || port, () => {
    console.log(`API is now online on port: ${process.env.PORT || port}`);
  });
}

// Exports
module.exports = app;

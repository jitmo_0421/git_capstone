import react from '@vitejs/plugin-react-swc';
// import dotenv from 'dotenv';
import { defineConfig } from 'vite';

// dotenv.config();

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    proxy: {
      '/api': {
        target: 'https://jre-real-estate.onrender.com',
        changeOrigin: true,
        secure: false,
      },
    },
  },
  plugins: [react()],
});

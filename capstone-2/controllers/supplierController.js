// Dependencies
const Suppliers = require('../models/Supplier.js');
const Products = require('../models/Products.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

// Add New Supplier
const addSupplier = async (req, res) => {
  try {
    const newSupplier = await new Suppliers({
      supplierName: req.body.supplierName,
      supplierEmail: req.body.supplierEmail,
      supplierPhone: req.body.supplierPhone,
      supplierAddress: {
        street: req.body.street,
        brgy: req.body.brgy,
        municipality: req.body.municipality,
        city: req.body.city,
        province: req.body.province,
      },
      supplierContactPerson: req.body.supplierContactPerson,
    });
    await Suppliers.find({ supplierName: req.body.supplierName }).then((supplier) => {
      if (supplier.length > 0) {
        res.status(400).send({ message: 'Supplier already exists' });
      } else {
        return newSupplier.save().then((supplier, err) => {
          if (err) {
            return res.status(400).json({ error: 'Internal Server Error' });
          } else {
            return res.status(201).json({ message: 'Supplier Added Successfully' });
          }
        });
      }
    });
  } catch (err) {
    return res.status(400).json({ error: err.message });
  }
};

// View All Suppliers
const viewSuppliers = async (req, res) => {
  try {
    const suppliers = await Suppliers.find();
    return res.status(200).json(suppliers);
  } catch (err) {
    return res.status(400).json({ error: err.message });
  }
};

// View Specific Supplier
const viewSpecificSupplier = async (req, res) => {
  try {
    const supplier = await Suppliers.findById(req.params.id);
    return res.status(200).json(supplier);
  } catch (err) {
    if (err.kind) {
      return res.status(404).send({ error: 'Supplier Not Found!' });
    } else {
      return res.status(400).send({ error: err.message });
    }
  }
};

// View Supplier Products
const viewSupplierProducts = async (req, res) => {
  try {
    const products = await Products.find({ 'supplier.supplierId': req.params.id });
    return res.status(200).json(products);
  } catch (err) {
    if (err.kind) {
      return res.status(404).send({ error: 'Product Not Found!' });
    } else {
      return res.status(400).send({ error: err.message });
    }
  }
};

// View Supplier Products Category
const viewSupplierCategory = async (req, res) => {
  try {
    const products = await Products.find({
      'supplier.supplierId': req.params.id,
      category: new RegExp(`^${req.params.category}$`, 'i'),
    });
    return res.status(200).json(products);
  } catch (err) {
    if (err.kind) {
      return res.status(404).send({ error: 'Category Not Found!' });
    } else {
      return res.status(400).send({ error: err.message });
    }
  }
};

module.exports = {
  addSupplier: addSupplier !== undefined ? addSupplier : null,
  viewSuppliers: viewSuppliers !== undefined ? viewSuppliers : null,
  viewSpecificSupplier: viewSpecificSupplier !== undefined ? viewSpecificSupplier : null,
  viewSupplierProducts: viewSupplierProducts !== undefined ? viewSupplierProducts : null,
  viewSupplierCategory: viewSupplierCategory !== undefined ? viewSupplierCategory : null,
};

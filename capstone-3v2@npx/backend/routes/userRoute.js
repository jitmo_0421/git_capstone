const express = require('express');

const { verifyToken, verifyAdmin } = require('../utils/token');

// Controller
const userController = require('../controllers/userController');

// Routing Components
const router = express.Router();

// Update User
router.post('/update/:id', verifyToken, userController.updateUser);

// Deactivate User
router.put('/delete/:id', verifyToken, userController.deleteUser);

// View Listings
router.get('/listings/:id', verifyToken, userController.getUserListings);

// Get All Users
router.get('/all', verifyToken, verifyAdmin, userController.getAllUsers);

// Change Listing State
router.put('/change/:action/:id', verifyToken, verifyAdmin, userController.changeState);

// View User
router.get('/view/profile/:id', verifyToken, userController.viewUser);

// Exports
module.exports = router;

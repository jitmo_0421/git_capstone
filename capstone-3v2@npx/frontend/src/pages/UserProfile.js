import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

export default function ViewProfile() {
  const params = useParams();
  const [user, setUser] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await fetch(
          `${import.meta.env.VITE_BACK_END_API_URL}/api/user/view/profile/${params.userId}`
        );
        const data = await res.json();
        setUser(data);
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  }, []);
  console.log(user.firstName);
  return (
    <div className="p-3 max-w-lg mx-auto">
      <div className="flex flex-col gap-4 my-5">
        <img
          src={user.avatar}
          alt="profile"
          className="rounded-full h-32 w-32 object-cover cursor-pointer self-center my-3"
        />
        <p>{user.firstName && user.lastName ? user.firstName + ' ' + user.lastName : user.displayName}</p>
        <p>
          <span className="font-semibold">Contact Number: {user.contactNumber} </span>
        </p>
        <p>
          <span className="font-semibold">Email: </span> {user.email}
        </p>
        <p>
          <span className="font-semibold">Username: </span> {user.username}
        </p>
        <div className="">
          <p className="text-green-500 font-semibold">
            <span className="cursor-pointer">Send Message</span>
          </p>
        </div>
      </div>
    </div>
  );
}

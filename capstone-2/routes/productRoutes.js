// Dependencies
const express = require('express');

// Routing Component
const router = express.Router();

// Controllers
const productController = require('../controllers/productController.js');
const auth = require('../auth.js');

// Destructure Auth
const { verify, verifyAdmin } = auth;

// Add Product
router.post('/add-product', verify, verifyAdmin, productController.addProduct);

// Get All Products
router.get('/all', productController.getAllProducts);

// Get Active Products
router.get('/active', productController.getActiveProducts);

// Get Archived Products
router.get('/archive', verify, verifyAdmin, productController.getArchivedProducts);

// Get Specific Product
router.post('/search', productController.searchProduct);

// Update Product
router.put('/:id/edit', verify, verifyAdmin, productController.updateProduct);

// Archive Product
router.put('/:id/archive', verify, verifyAdmin, productController.archiveProduct);

// Activate Product
router.put('/:id/activate', verify, verifyAdmin, productController.activateProduct);

// View Products by Category
router.get('/:category', productController.viewProductsByCategory);

// View Products by Subcategory
router.get('/:category/:subcategory', productController.viewProductsBySub);

module.exports = router;

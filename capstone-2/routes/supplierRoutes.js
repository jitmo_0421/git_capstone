// Dependencies
const express = require('express');

// Routing Component
const router = express.Router();

// Controllers
const supplierController = require('../controllers/supplierController.js');
const auth = require('../auth.js');

// Destructure auth
const { verify, verifyAdmin } = auth;

// Add Supplier Route
router.post('/add', verify, verifyAdmin, supplierController.addSupplier);

// View All Supplier Route
router.get('/all', verify, verifyAdmin, supplierController.viewSuppliers);

// View Specific Supplier
router.get('/:id/view', verify, verifyAdmin, supplierController.viewSpecificSupplier);

// View Supplier Products
router.get('/:id/products', verify, verifyAdmin, supplierController.viewSupplierProducts);

// View Supplier Products Category
router.get('/:id/products/:category', verify, verifyAdmin, supplierController.viewSupplierCategory);

module.exports = router;

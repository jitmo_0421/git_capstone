// Dependencies
const jwt = require('jsonwebtoken');
require('dotenv').config();

// SECRET key from .env
const SECRET_KEY = process.env.KEY;

// Create Token
const createAccessToken = (user) => {
  const userData = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };
  return jwt.sign(userData, SECRET_KEY, {});
};

// Retreive token from the request header
const verify = (req, res, next) => {
  // Auth Type is Bearer Token
  console.log(req.headers.authorization);

  let token = req.headers.authorization;

  if (typeof token === 'undefined') {
    return res.send({ auth: 'Failed, No Token!' });
  } else {
    console.log(token);
    token = token.slice(7, token.length);

    console.log(token);

    jwt.verify(token, SECRET_KEY, function (error, decodedToken) {
      if (error) {
        return res.send({
          auth: 'Failed',
          message: error.message,
        });
      } else {
        console.log(decodedToken);
        req.user = decodedToken;

        // Will let the controller to proceed next
        next();
      }
    });
  }
};

// Verify User Level
const verifyAdmin = (req, res, next) => {
  if (req.user.isAdmin) {
    next();
  } else {
    return res.send({
      auth: 'Failed!',
      message: 'Access Forbidden',
    });
  }
};

module.exports = {
  createAccessToken: createAccessToken !== undefined ? createAccessToken : null,
  verify: verify !== undefined ? verify : null,
  verifyAdmin: verifyAdmin !== undefined ? verifyAdmin : null,
};

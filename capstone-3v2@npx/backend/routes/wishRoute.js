const express = require('express');
const wishController = require('../controllers/wishController');
const { verifyToken } = require('../utils/token');

const router = express.Router();

//Add to Wish
router.post('/wish/:id', verifyToken, wishController.addWish);

//Get wishlist
router.get('/wish/view', verifyToken, wishController.getWishlist);

//Purchase Wish
router.post('/wish/create/:id', verifyToken, wishController.purchaseWish);

//Delete Wish
router.delete('/wish/remove/:id', verifyToken, wishController.deleteWish);

module.exports = router;

// Server Dependencies
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// Environment Variable
require('dotenv').config();

//Access Routes
const userRoutes = require('./routes/userRoutes.js'); // User Routes
const productRoutes = require('./routes/productRoutes.js'); // Product Routes
const cartRoutes = require('./routes/cartRoutes.js'); // Cart Routes
const orderRroutes = require('./routes/orderRoutes.js'); // Order Routes
const supplierRoutes = require('./routes/supplierRoutes.js'); // Supplier Routes

// Server Setup and Routes
const port = 4000; // Sub Port

const app = express();
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());

// Database Connection
mongoose.connect(process.env.dbURL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

// Database Checkpoints
const dbConnection = mongoose.connection;
dbConnection.on('error', console.error.bind(console, 'connection error:'));
dbConnection.once('open', () => {
  console.log('Connected to Atlas Cloud Database');
});

// Homepage
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

// Back-end Routes
app.use('/users', userRoutes); // User Routes End-Point
app.use('/products', productRoutes); // Product Routes End-Point
app.use('/cart', cartRoutes); // Cart Routes End-Point
app.use('/orders', orderRroutes); // Order Routes End-Point
app.use('/suppliers', supplierRoutes); // Supplier Routes End-Point

// Server Gateway Reponse
if (require.main === module) {
  app.listen(process.env.PORT || port, () => {
    console.log(`API is now online on port: ${process.env.PORT || port}`);
  });
}

// Exports
module.exports = app;

const express = require('express');
const purchaseController = require('../controllers/purchaseController');
const { verifyToken } = require('../utils/token');

const router = express.Router();

// Create Purchase
router.post('/create/:productId', verifyToken, purchaseController.createPurchase);

// Get Orders
router.get('/view', verifyToken, purchaseController.viewOrders);

// Get User Orders
router.get('/transactions', verifyToken, purchaseController.getUserOrders);

// Update Order Status
router.put('/update/:id', verifyToken, purchaseController.updateState);

module.exports = router;

const User = require('../models/User');
const bcryptjs = require('bcryptjs');
const token = require('../utils/token');
const { errorHandler } = require('../utils/error');

// User signup
const signup = async (req, res, next) => {
  const { firstName, lastName, username, email, password, contactNumber } = req.body;
  const displayName = `${firstName} ${lastName}`;
  const hashedPassword = bcryptjs.hashSync(password, 10);
  const newUser = new User({
    firstName: firstName,
    lastName: lastName,
    displayName: displayName,
    username: username,
    email: email,
    password: hashedPassword,
    contactNumber: contactNumber,
  });
  try {
    await newUser.save();
    res.status(201).json('user created successfully');
  } catch (error) {
    // next(errorHandler(550, 'error from the function'));
    next(error);
  }
};

// User Sign in
const signin = async (req, res, next) => {
  const { email, password } = req.body;
  try {
    const checkUser = await User.findOne({ email }, { __v: 0, createdAt: 0, updatedAt: 0 });
    if (!checkUser) return next(errorHandler(404, 'User not found!'));

    if (checkUser.isActive === false)
      return res.status(401).json({ success: false, message: 'User is currently not active anymore!' });

    const validPassword = bcryptjs.compareSync(password, checkUser.password);
    if (!validPassword) return next(errorHandler(401, 'Wrong credentials'));

    const tokie = token.createAccessToken(checkUser);
    const { password: pass, ...rest } = checkUser._doc;

    res
      .cookie('access_token', tokie, { httpOnly: true, sameSite: 'none', secure: 'false' })
      .status(200)
      .json(rest);
  } catch (error) {
    next(error);
  }
};

// Google Signup
const google = async (req, res, next) => {
  try {
    const userCheck = await User.findOne({ email: req.body.email });
    if (userCheck) {
      const tokie = token.createAccessToken(userCheck);
      const { password: pass, ...rest } = userCheck._doc;
      res
        .cookie('access_token', tokie, { httpOnly: true, sameSite: 'none', secure: 'false' })
        .status(200)
        .json(rest);
    } else if (userCheck.isActive === false) {
      return res.status(401).json({ success: false, message: 'User is currently not active anymore!' });
    } else {
      const generatedPassword = Math.random().toString(36).slice(-8) + Math.random().toString(36).slice(-8);
      const hashPassword = bcryptjs.hashSync(generatedPassword, 10);
      const username = req.body.name.split(' ').join('').toLowerCase() + Math.random().toString(36).slice(-4);
      const newUser = new User({
        displayName: req.body.name,
        username: username,
        email: req.body.email,
        password: hashPassword,
        avatar: req.body.photo,
      });
      const result = await newUser.save();
      const tokie = token.createAccessToken(result);
      const { password: pass, ...rest } = result._doc;
      res
        .cookie('access_token', tokie, { httpOnly: true, sameSite: 'none', secure: 'false' })
        .status(200)
        .json(rest);
    }
  } catch (error) {
    next(error);
  }
};

// Sign Out
const signOut = (req, res, next) => {
  try {
    res.clearCookie('access_token');
    res.status(200).json('User has been logged out!!');
  } catch (error) {
    next(error);
  }
};

// Exports
module.exports = {
  signup: signup !== undefined ? signup : null,
  signin: signin !== undefined ? signin : null,
  google: google !== undefined ? google : null,
  signOut: signOut !== undefined ? signOut : null,
};
